# lunatech_imdb

## Steps to run the application

1. Install Docker: https://docs.docker.com/install/

2. Install Docker Compose: https://docs.docker.com/compose/install/

3. Clone or download this project or use the zip file shared through 
email.
    * Go to terminal or powershell depending of your OS.  
    * CD to the project folder.
    * It assumes that port `8080`, `8088`and `3306` are available to use.
    * Build and package the project and move the jar file out of the target folder.
        * mvn clean compile
        * mvn package
        * mv target/*.jar ../lunatech_imdb/
        * Build app image: `docker-compose build`.
        * Run the following command and start the app server: 
            * `docker-compose up db`
            * `docker-compose up app`
4. To download and populate the data
    * Go to terminal or powershell depending of your OS.  
    * CD to the project folder.
    * chmod 755 download_script.sh
    * Run `./download_script.sh` 
    * Go to mysql environment and run `source insert.sql`.
    (This will take some time as the dataset is big) 

# API Details
1. For top rated movies API:
    * URL: http://localhost:8080/genres/Horror
    * This by default will return 50 results
    * If you want less or more than we need to pass page no and size through query param.
    i.e. http://localhost:8080/genres/Horror?page=0&size=100
2.  For typecating API:
    * URL: http://localhost:8080/typecast/Kevin%20Bacon
3. For six degree of bacon API:
    * URL: http://localhost:8080/degree/Sean%20Penn
   