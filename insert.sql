USE imdb;
LOAD DATA LOCAL INFILE 'name.basics.tsv' INTO TABLE name_basics;
LOAD DATA LOCAL INFILE 'title.basics.tsv' INTO TABLE title_basics;
LOAD DATA LOCAL INFILE 'title.principals.tsv' INTO TABLE title_principals;
LOAD DATA LOCAL INFILE 'title.ratings.tsv' INTO TABLE title_ratings;