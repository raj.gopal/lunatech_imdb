curl https://datasets.imdbws.com/name.basics.tsv.gz --output name.basics.tsv.gz
curl https://datasets.imdbws.com/title.basics.tsv.gz --output title.basics.tsv.gz
curl https://datasets.imdbws.com/title.principals.tsv.gz --output title.principals.tsv.gz
curl https://datasets.imdbws.com/title.ratings.tsv.gz --output title.ratings.tsv.gz

# gunzip should be installed
gunzip --keep name.basics.tsv.gz
gunzip --keep title.basics.tsv.gz
gunzip --keep title.principals.tsv.gz
gunzip --keep title.ratings.tsv.gz

rm -rf name.basics.tsv.gz
rm -rf title.basics.tsv.gz
rm -rf title.principals.tsv.gz
rm -rf title.ratings.tsv.gz