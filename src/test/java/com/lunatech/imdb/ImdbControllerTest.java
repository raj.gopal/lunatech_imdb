package com.lunatech.imdb;

import com.lunatech.imdb.controller.ImdbController;
import com.lunatech.imdb.entity.Basic;
import com.lunatech.imdb.entity.Name;
import com.lunatech.imdb.model.Movie;
import com.lunatech.imdb.model.TypecastResult;
import com.lunatech.imdb.repository.MovieRepository;
import com.lunatech.imdb.service.DegreeService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.PageRequest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class ImdbControllerTest {

    @InjectMocks
    ImdbController imdbController;

    @Mock
    MovieRepository movieRepository;

    @Mock
    DegreeService degreeService;

    @Test
    public void testGetMovieGenre(){
        Movie m1 = new Movie("t1", "titlehorror1", "primaryhorror1", "2000", "Horror", "9.5", "50000");
        Movie m2 = new Movie("t2", "titlehorror2", "primaryhorror2", "2001", "Horror", "9.2", "50000");
        List<Movie> movieList = Arrays.asList(new Movie[]{m1, m2});

        when(movieRepository.searchMovieByGenre("Horror", PageRequest.of(0,10))).thenReturn(movieList);

        String genre = "Horror";
        List<Movie> getMovieByGenre = imdbController.getMoviesGenre(genre,0,10);

        assertThat(getMovieByGenre.get(0).getGenres()).isEqualTo(genre);
        assertThat(getMovieByGenre.get(1).getGenres()).isEqualTo(genre);
    }

    @Test
    public void testGetTypecast(){
        Name n1 = new Name("nm0000001","Fred Astaire",1899,1987,"soundtrack,actor,miscellaneous","tt0072308,tt0053137,tt0043044,tt0050419");
        List<String> nameList = Collections.singletonList(n1.getKnownForTitles());
        when(movieRepository.getActorDetail("Fred Astaire")).thenReturn(nameList);

        List<String> nameListString = nameList.stream()
                .map(t -> t.split(","))
                .flatMap(tl -> Arrays.stream(tl))
                .collect(Collectors.toList());

        Basic b1 = new Basic("tt0043044","movie","Three Little Words","Three Little Words","0","1950","1960","102","Biography,Comedy,Musical");
        Basic b2 = new Basic("tt0050419","movie","Funny Face","Funny Face","0","1957","1960","103","Comedy,Musical,Romance");
        Basic b3 = new Basic("tt0053137","movie","On the Beach","On the Beach","0","1959","1960","134","Drama,Romance,Sci-Fi");
        Basic b4 = new Basic("tt0072308","movie","The Towering Inferno","The Towering Inferno","0","1974","1980","165","Action,Drama,Thriller");
        List<Basic> basicList = Arrays.asList(new Basic[] {b1,b2,b3,b4});
        when(movieRepository.getBasics(nameListString)).thenReturn(basicList);

        TypecastResult typecastResult = imdbController.getTypecast("Fred Astaire");

        assertThat(typecastResult.typecasted).isEqualTo(Boolean.TRUE);
        assertThat(typecastResult.genreTypecasted).isNotEmpty();
    }

    @Test
    public void testGetDegreeOfSeparation(){
        when(degreeService.getDegree("Sean Penn")).thenReturn(1);

        int degree = imdbController.getDegreeOfSeparation("Sean Penn");
        assertThat(degree == 1);
    }
}