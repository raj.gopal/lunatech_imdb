package com.lunatech.imdb;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = ImdbApplication.class)
@AutoConfigureMockMvc
public class ImdbApplicationIntegrationTest {

    @Autowired
    private MockMvc mvc;

    @Test
    public void testGetMovieGenre() throws Exception {
        this.mvc.perform(get("/genres/Horror?page=0&size=10").contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(greaterThanOrEqualTo(3))))
                .andExpect(jsonPath("$[0].genres", containsString("Horror")))
                .andExpect(jsonPath("$[1].genres", containsString("Horror")))
                .andExpect(jsonPath("$[2].genres", containsString("Horror")))
                .andExpect(jsonPath("$[3].genres", containsString("Horror")));
    }

    @Test
    public void testGetMovieGenreNull() throws Exception {
        this.mvc.perform(get("/genres/unkown genre?page=0&size=10").contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(greaterThanOrEqualTo(0))));
    }

    @Test
    public void testGetTypecast() throws Exception {
        this.mvc.perform(get("/typecast/Fred Astaire").contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.typecasted", is(Boolean.TRUE)));
    }

    @Test
    public void testGetTypecastNull() throws Exception {
        this.mvc.perform(get("/typecast/unkown actor").contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.typecasted", is(Boolean.FALSE)));
    }

    @Test
    public void testGetDegreeOfSeparation() throws Exception {
        this.mvc.perform(get("/degree/Sean Penn").contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", is(1)));
    }

    @Test
    public void testGetDegreeOfSeparationNegative() throws Exception {
        this.mvc.perform(get("/degree/unkown actor").contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", is(-1)));
    }

    @Test
    public void testGetDegreeOfSeparationPositive() throws Exception {
        this.mvc.perform(get("/degree/Kevin Bacon").contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", is(0)));
    }
}