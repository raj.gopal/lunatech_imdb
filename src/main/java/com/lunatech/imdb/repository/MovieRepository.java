package com.lunatech.imdb.repository;

import com.lunatech.imdb.entity.Basic;
import com.lunatech.imdb.model.Movie;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MovieRepository extends CrudRepository<Basic, String> {

    /**
     * Query to get a list of top rated movies in a particular genre
     * @param genre value for genre
     */
    @Query("SELECT new com.lunatech.imdb.model.Movie" +
            "(b.tconst, b.titleType, b.primaryTitle, b.startYear, b.genres, r.averageRating, r.numVotes) " +
            "FROM Basic b JOIN Rating r ON b.tconst = r.tconst WHERE b.titleType = 'movie' " +
            "AND b.genres LIKE %:genre% ORDER BY r.averageRating DESC")
    List<Movie> searchMovieByGenre(@Param("genre") String genre,  Pageable pageable);

    /**
     * Query to get list of movies for which an actor is known
     * An actor can have more than 1 entry in Database
     * knownForTitles = comma separated movie ids (tconst)
     */
    @Query("SELECT n.knownForTitles FROM Name n  WHERE n.primaryName = :name " +
            "AND (n.primaryProfession  LIKE '%actor%' OR n.primaryProfession LIKE '%actress%')")
    List<String> getActorDetail(@Param("name") String name);

    @Query(value = "SELECT b FROM Basic b WHERE b.tconst IN :titles")
    List<Basic> getBasics(@Param("titles") List<String> titles);

    /**
     * Given a list of movie titles (movie ids) return list of actors
     * @param titles list of movie ids
     * @return list of actors
     */
    @Query("SELECT p.nconst FROM Principal p WHERE p.tconst IN :titles and (category='actor' or category='actress')")
    List<String> getActorIds(@Param("titles") List<String> titles);

    /**
     * Given a list of actor ids returns actor names
     * @param actorIds list of actor ids
     * @return list of actor names
     */
    @Query("SELECT n.primaryName FROM Name n WHERE n.nconst IN :name")
    List<String> getActorName(@Param("name")List<String> actorIds);
}