package com.lunatech.imdb.controller;

import com.lunatech.imdb.entity.Basic;
import com.lunatech.imdb.model.Movie;
import com.lunatech.imdb.model.TypecastResult;
import com.lunatech.imdb.repository.MovieRepository;
import com.lunatech.imdb.service.DegreeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@RestController
public class ImdbController {

    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private DegreeService degreeService;

    /**
     * API for getting top rated movie in a particular genre
     * @param genre name of genre
     * @param page pagination param
     * @param size pagination param
     * @return a paignated result of the top
     * rated movie in a particualr genre.
     */
    @GetMapping("/genres/{genre}")
    public List<Movie> getMoviesGenre(@PathVariable String genre,
                                      @RequestParam(defaultValue = "0", name = "page") int page,
                                      @RequestParam(defaultValue = "50", name = "size") int size){
        List<Movie> movies = movieRepository.searchMovieByGenre(genre, PageRequest.of(page, size));
        return movies;
    }

    /**
     * API for checking whether an actor is typecasted
     * (half or more of the work is in same genre) or not
     * @param name name of actor
     * @return an object of TypecastResult which contains
     * a list of genres where the actor is typecasted
     * and a boolean value which says if the actor is typecasted or not.
     */
    @GetMapping("/typecast/{name}")
    public TypecastResult getTypecast(@PathVariable String name) {
        List<String> actorList = movieRepository.getActorDetail(name);

        // list of all the titles the actor/actress has played
        List<String> titleList = actorList.stream()
                .map(t -> t.split(","))
                .flatMap(tl -> Arrays.stream(tl))
                .collect(Collectors.toList());

        List<Basic> getAllBasicDetails = movieRepository.getBasics(titleList);

        // count for half of the work he has done
        int halfWork = titleList.size()/2;
        Map<String, Long> genreGroupByCount = getGenresGroupByCount(getAllBasicDetails);
        return IsTypeCasted(genreGroupByCount, halfWork);
    }

    /**
     * Method to determine if the map of genres by count contains
     * any work of the actor which is more than half of his/her work
     * @param genreGroupByCount map of genres by their count
     * @param halfWork value of half work done by the actor
     * @return an object of TypecastResult which contains
     * a list of genres where the actor is typecasted
     * and a boolean value which says if the actor is typecasted or not.
     */
    private TypecastResult IsTypeCasted(Map<String, Long> genreGroupByCount, int halfWork) {
        List<TypecastResult> isTypecastedResults =
                genreGroupByCount
                .entrySet()
                .stream()
                .filter(e -> e.getValue() >= halfWork)
                .map(e -> new TypecastResult(true, e.getKey()))
                .collect(Collectors.toList());

        if (isTypecastedResults.size() == 1) {
            return isTypecastedResults.get(0);
        } else if (isTypecastedResults.size() > 1) {
            // Note: there is an extreme such as, 200 action genre appearances and 200 drama genre appearances
            return new TypecastResult(
                    true,
                    isTypecastedResults.stream().map(r -> r.genreTypecasted).collect(Collectors.joining(","))
            );
        } else {
            return new TypecastResult(false, null);
        }
    }

    /**
     * Method to get genres group by count
     * @param basicList List of title_basics
     * @return a map of genres and their count
     */
    private Map<String, Long> getGenresGroupByCount(List<Basic> basicList){
        return basicList.stream()
                .map(Basic::getGenres)
                .filter(Objects::nonNull)
                .map(genres -> Arrays.asList(genres.split(",")))
                .flatMap(Collection::stream)
                .collect(Collectors.groupingBy(Function.identity(),Collectors.counting()));
    }

    /**
     * API for getting the minimum degree of Kevin Bacon for any actor
     * @param name Actor whose degree of Kevin Bacon needs to be determined
     * @return minimum degree of Kevin Bacon
     */
    @GetMapping("/degree/{name}")
    public int getDegreeOfSeparation(@PathVariable String name) {
        return degreeService.getDegree(name);
    }
}
