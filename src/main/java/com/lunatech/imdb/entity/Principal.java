package com.lunatech.imdb.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Data
@Table(name="title_principals")
public class Principal
{
    @Id
    private String tconst;
    private String ordering;
    private String nconst;
    private String category;
    private String job;
    private String characters;
}