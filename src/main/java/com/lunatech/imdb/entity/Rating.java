package com.lunatech.imdb.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name="title_ratings")
public class Rating {
    @Id
    private String tconst;

    @Column(name = "average_rating")
    private String averageRating;

    @Column(name = "num_votes")
    private String numVotes;
}