package com.lunatech.imdb.service;

import com.lunatech.imdb.repository.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class DegreeService {

    @Autowired
    private MovieRepository movieRepository;

    /**
     * Method which determines the minimum degree of Kevin Bacon
     * @param name name of the actor/actress
     * @return degree of Kevin Bacon
     */
    public int getDegree(String name) {
        Map<String, Integer> actorLevelMap = new HashMap<>();
        List<String> actors = new LinkedList<>();
        if (name.equals("Kevin Bacon"))
            return 0;

        List<String> coActors = getCoActors(name);
        boolean isKbCoActor = isCoActor(coActors);
        if(isKbCoActor)
            return 1;
        else {
            actors.addAll(coActors);
            actorLevelMap.put(name, 0);
            actors.forEach(actor -> actorLevelMap.put(actor, 1));
        }

        ListIterator<String> actorsIter = actors.listIterator();
        while(actorsIter.hasNext()) {
            String coActor = actorsIter.next();
            coActors = getCoActors(coActor);
            isKbCoActor = isCoActor(coActors);
            int actorLevel = actorLevelMap.get(coActor);
            System.out.println(actorLevel + " : " + coActor);
            if(isKbCoActor)
                return actorLevel + 1;
            else {
                coActors.stream()
                        .filter(actor -> !actorLevelMap.containsKey(actor)) // remove any already processed actor
                        .forEach(actor -> {
                            actorsIter.add(actor); // add in the actor list using iterator
                            actorLevelMap.put(actor, actorLevel + 1); // add in the map with its level
                        });
            }
        }
        return -1;
    }

    /**
     * Method to check if a list of actor contains Kevin bacon
     * @param coActor List of actor/actress
     * @return a boolean value if the list contains Kevin Bacon or not.
     */
    private boolean isCoActor(List<String> coActor){
        return coActor.stream().anyMatch(actor -> actor.equals("Kevin Bacon"));
    }

    /**
     * Method to get name of all the co actors of an actor/actress
     * @param name name of the actor/actress
     * @return List of coActors
     */
    private List<String> getCoActors(String name){
        List<String> movieTitles = movieRepository.getActorDetail(name);

        // list of all the titles the actor/actress has played
        List<String> titleList = movieTitles.stream()
                .filter(Objects::nonNull)
                .map(t -> t.split(","))
                .flatMap(Arrays::stream)
                .distinct() // remove duplicate titles
                .collect(Collectors.toList());

        List<String> coActorIds = movieRepository.getActorIds(titleList)
                .stream()
                .distinct() // remove same actors from different titles
                .collect(Collectors.toList());

        return movieRepository.getActorName(coActorIds).stream()
                .filter((actorName -> !actorName.equalsIgnoreCase(name))) // remove the current actor;
                .collect(Collectors.toList());
    }
}