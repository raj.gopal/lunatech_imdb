package com.lunatech.imdb.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Movie {
    private String tconst;
    private String titleType;
    private String primaryTitle;
    private String startYear;
    private String genres;
    private String averageRating;
    private String numVotes;
}
