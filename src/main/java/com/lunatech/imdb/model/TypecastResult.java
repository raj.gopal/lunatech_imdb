package com.lunatech.imdb.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TypecastResult {
    public String genreTypecasted;
    public boolean typecasted;

    public TypecastResult(boolean b, String key) {
        this.typecasted = b;
        this.genreTypecasted = key;
    }
}
